rmdir /s /q built
docker build . -f builders/windows/Dockerfile -t tile-puzzle-windows --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name tile-puzzle-windows tile-puzzle-windows
docker cp tile-puzzle-windows:/game/built built
docker rm tile-puzzle-windows
cd built
tile-puzzle.exe
