#!/bin/sh
set -eux
rm -rf built
docker build . -f builders/ubuntu/Dockerfile -t tile-puzzle-ubuntu --build-arg CI_COMMIT_BRANCH="don't push" --build-arg DEBUG="-debug"
docker create --name tile-puzzle-ubuntu tile-puzzle-ubuntu
docker cp tile-puzzle-ubuntu:/game/built built
docker rm tile-puzzle-ubuntu
cd built
./tile-puzzle
