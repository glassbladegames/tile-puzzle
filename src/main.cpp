#include <slice.h>
#include <sliceopts.h>
#include <slicefps.h>
bool ShowFPS = false;
void ToggleFPS ()
{
	ShowFPS = !ShowFPS;
	fpsSetIndicatorEnabled(ShowFPS);
};
bool* HorizontalConns;
bool* VerticalConns;
slBU GridW;
slBU GridH;
#define tilepath_empty NULL
#define tilepath_stub "resource/stub.png"
#define tilepath_straight "resource/straight.png"
#define tilepath_3 "resource/3.png"
#define tilepath_4 "resource/4.png"
#define tilepath_corner "resource/corner.png"
Uint8* tileids;
#define tileid_empty 0
#define tileid_stub 1
#define tileid_straight 2
#define tileid_3 3
#define tileid_4 4
#define tileid_corner 5
char* CalcTile (slBS x, slBS y, Uint8* id)
{
	bool left,right,top,bottom;
	if (x) left = HorizontalConns[(GridW - 1) * y + (x - 1)];
	else left = false;
	if (x + 1 == GridW) right = false;
	else right = HorizontalConns[(GridW - 1) * y + x];
	if (y) top = VerticalConns[GridW * (y - 1) + x];
	else top = false;
	if (y + 1 == GridH) bottom = false;
	else bottom = VerticalConns[GridW * y + x];
	if (left && top && right && bottom)
	{
		*id = tileid_4;
		return tilepath_4;
	};
	if (left && top && right)
	{
		*id = tileid_3;
		return tilepath_3;
	};
	if (top && right && bottom)
	{
		*id = tileid_3;
		return tilepath_3;
	};
	if (right && bottom && left)
	{
		*id = tileid_3;
		return tilepath_3;
	};
	if (bottom && left && top)
	{
		*id = tileid_3;
		return tilepath_3;
	};
	if (left && right)
	{
		*id = tileid_straight;
		return tilepath_straight;
	};
	if (top && bottom)
	{
		*id = tileid_straight;
		return tilepath_straight;
	};
	if (left && top)
	{
		*id = tileid_corner;
		return tilepath_corner;
	};
	if (top && right)
	{
		*id = tileid_corner;
		return tilepath_corner;
	};
	if (right && bottom)
	{
		*id = tileid_corner;
		return tilepath_corner;
	};
	if (bottom && left)
	{
		*id = tileid_corner;
		return tilepath_corner;
	};
	if (left)
	{
		*id = tileid_stub;
		return tilepath_stub;
	};
	if (top)
	{
		*id = tileid_stub;
		return tilepath_stub;
	};
	if (right)
	{
		*id = tileid_stub;
		return tilepath_stub;
	};
	if (bottom)
	{
		*id = tileid_stub;
		return tilepath_stub;
	};
	*id = tileid_empty;
	return tilepath_empty;
};
void GenConns ()
{
	slBU cur;
	slBU total = (GridW - 1) * GridH;
	for (cur = 0; cur < total; cur++) HorizontalConns[cur] = rand() & 1;
	total = GridW * (GridH - 1);
	for (cur = 0; cur < total; cur++) VerticalConns[cur] = rand() & 1;
};
Uint8* rots;
slBox** boxes;
slBox** clickboxes;
slBox* WinBox;
slBox* NextBox;
struct sides
{
	bool left,top,right,bottom;
};
sides GetSides (Uint8 id, Uint8 rot)
{
	bool L,T,R,B;
	switch (id)
	{
		case tileid_empty:
		L = false; T = false; R = false; B = false;
		break;
		case tileid_stub:
		L = false; T = true; R = false; B = false;
		break;
		case tileid_straight:
		L = false; T = true; R = false; B = true;
		break;
		case tileid_3:
		L = true; T = true; R = true; B = false;
		break;
		case tileid_4:
		L = true; T = true; R = true; B = true;
		break;
		case tileid_corner:
		L = true; T = true; R = false; B = false;
	};
	switch (rot)
	{
		case 0:
		return {L,T,R,B};
		case 1:
		return {B,L,T,R};
		case 2:
		return {R,B,L,T};
		case 3:
		return {T,R,B,L};
	};
};
struct Rotator
{
	slBox* box;
	slScalar target,start,age;
	slBU _index_;
    int pos;
};
slList Rotators("Rotators",offsetof(Rotator,_index_),false);
bool WinConfirmed;
void ConfirmWin ()
{
	if (WinBox && !Rotators.itemcount) WinConfirmed = true;
};
Uint64 timerend;
Uint64 timerstart;
void CheckWin ()
{
	for (slBU y = 0; y < GridH; y++) for (slBU x = 0; x < GridW; x++)
	{
		sides piece = GetSides(tileids[GridW * y + x],rots[GridW * y + x]);
		bool left,top;
		if (x) left = HorizontalConns[(GridW - 1) * y + (x - 1)];
		else left = false;
		if (y) top = VerticalConns[GridW * (y - 1) + x];
		else top = false;
		if (piece.left != left) return;
		if (piece.top != top) return;
		if (x + 1 == GridW)
		{
			if (piece.right) return;
		}
		else HorizontalConns[(GridW - 1) * y + x] = piece.right;
		if (y + 1 == GridH)
		{
			if (piece.bottom) return;
		}
		else VerticalConns[GridW * y + x] = piece.bottom;
	};
	WinBox = slCreateBox();
	slSetBoxDims(WinBox,0,0,1,1,255);
	WinBox->backcolor = slHSVAtoRGBA({rand() & 0xFF,0xBF,0x7F,0});
	slBU total = GridW * GridH;
	for (slBU cur = 0; cur < total; cur++) clickboxes[cur]->onclick = NULL;
	NextBox = slCreateBox();
	slSetBoxDims(NextBox,0,0,1,1,150);
	NextBox->onclick = ConfirmWin;
	timerend = SDL_GetPerformanceCounter();
};
void SpawnRotator (slBox* box, int pos, slScalar angle_add)
{
	Rotator* out = box->userdata;
    if (!out)
    {
    	out = malloc(sizeof(Rotator));
    	Rotators.Add(out);
    	out->box = box;
        box->userdata = out;

        out->target = box->rotangle;
        out->pos = pos;
    }

	out->target += angle_add;
	out->start = box->rotangle;
	out->age = 0;
};
slBox* timerbox;
void RotateBox_Common (slBox* clickbox, int rot_add)
{
	if (WinBox) return;
	if (!timerbox)
	{
		timerbox = slCreateBox();
		slSetBoxDims(timerbox,0,0.025,1,0.05,100);
		timerstart = SDL_GetPerformanceCounter();
	};
    slBU pos = clickbox->userdata;
    rots[pos] = ((rots[pos] | 4) + rot_add) & 3;
	SpawnRotator(boxes[pos], pos, rot_add * 90);
	CheckWin();
}
void RotateBox_CW (slBox* clickbox)
{
    RotateBox_Common(clickbox,1);
};
void RotateBox_CCW (slBox* clickbox)
{
    RotateBox_Common(clickbox,-1);
};
void StepRotators ()
{
	for (slBU cur = 0; cur < Rotators.itemcount; cur++)
	{
		Rotator* rotator = Rotators.items[cur];
		rotator->age += slGetDelta() * 3;
		if (rotator->age >= 1)
		{
			rotator->box->rotangle = rots[rotator->pos] * 90;
            rotator->box->userdata = NULL;
			Rotators.Remove(rotator);
			free(rotator);
			cur--;
			continue;
		};
		rotator->box->rotangle = rotator->target - (rotator->target - rotator->start) * (cos(rotator->age * M_PI) * .5 + .5);
	};
};
Uint8 min_grid_size,max_grid_size;
slSlider* min_size_slider;
slSlider* max_size_slider;
void on_minmax_change (slSlider* slider)
{
	Uint8 sizeval = slider->curvalue + 0.5;
	char* nstr;
	asprintf(&nstr,"%u",(unsigned int)sizeval);
	slider->mark->SetTexRef(slRenderText(nstr));
	free(nstr);
	if (slider == min_size_slider)
	{
		min_grid_size = sizeval;
		min_size_slider->SetValue(min_grid_size);
		if (min_grid_size > max_grid_size)
		{
			max_size_slider->SetValue(min_grid_size);
			on_minmax_change(max_size_slider);
		}
	}
	else
	{
		max_grid_size = sizeval;
		max_size_slider->SetValue(max_grid_size);
		if (max_grid_size < min_grid_size)
		{
			min_size_slider->SetValue(max_grid_size);
			on_minmax_change(min_size_slider);
		}
	}
}
slBox* settings_panel;
slBox* settings_title;
slBox* min_size_info;
slBox* min_size_back;
slBox* min_size_mark;
slBox* max_size_info;
slBox* max_size_back;
slBox* max_size_mark;
slBox* settings_close;
bool settings_visi;
void settings_set_visi (bool visi)
{
	settings_panel->visible = visi;
	settings_title->visible = visi;
	min_size_info->visible = visi;
	min_size_back->visible = visi;
	min_size_mark->visible = visi;
	max_size_info->visible = visi;
	max_size_back->visible = visi;
	max_size_mark->visible = visi;
	settings_close->visible = visi;
	settings_visi = visi;
};
bool op_try_escape ()
{
	if (settings_visi)
	{
		settings_set_visi(false);
		return true;
	}
	else return false;
};
bool SkipReq;
void OnSkipReq () { SkipReq = true; };
void CloseSettings () { settings_set_visi(false); };
int main ()
{
	slInit("Tile Puzzle");
	opInit();
	fpsInit();
	//slGetKeyBind("Next Puzzle",slMouseButton(1))->onpress = ConfirmWin;

	FILE* gridinfo = fopen("gridinfo","rb");
	if (gridinfo)
	{
		fread(&min_grid_size,1,1,gridinfo);
		fread(&max_grid_size,1,1,gridinfo);
		fclose(gridinfo);
	}
	else
	{
		min_grid_size = 6;
		max_grid_size = 12;
	};

	settings_panel = slCreateBox();
	slSetBoxDims(settings_panel,0.2,0.35,0.6,0.3,3);
	settings_panel->bordercolor = {255,255,255,255};
	settings_panel->backcolor = {0,0,0,255};
	settings_panel->onclick = slDoNothing;
	slBoxHoverAbsorb(settings_panel);

	settings_title = slCreateBox(slRenderText("Change Difficulty Settings"));
	slSetBoxDims(settings_title,0,0.05,1,0.15,2);
	slRelBoxDims(settings_title,settings_panel);



	min_size_info = slCreateBox(slRenderText("Grid Minimum X/Y"));
	slSetBoxDims(min_size_info,0,0.25,1,0.075,2);
	slRelBoxDims(min_size_info,settings_panel);

	min_size_back = slCreateBox();
	slSetBoxDims(min_size_back,0.1,0.325,0.8,0.1,2);
	slRelBoxDims(min_size_back,settings_panel);

	min_size_mark = slCreateBox();
	slSetBoxDims(min_size_mark,0,0,0.1,1,1);
	slRelBoxDims(min_size_mark,min_size_back);
	min_size_mark->backcolor = {0,0,0,255};
	min_size_mark->bordercolor = {255,255,255,255};

	min_size_back->xy.y += min_size_back->wh.h * 0.5f;
	min_size_back->wh.h = 0;
	min_size_back->bordercolor = {255,255,255,255};
	min_size_back->backcolor = {255,255,255,255};

	min_size_slider = slCreateSlider(min_size_back,min_size_mark);
	min_size_slider->onchange = on_minmax_change;
	min_size_slider->minvalue = 6;
	min_size_slider->maxvalue = 40;



	max_size_info = slCreateBox(slRenderText("Grid Maximum X/Y"));
	slSetBoxDims(max_size_info,0,0.5,1,0.075,2);
	slRelBoxDims(max_size_info,settings_panel);

	max_size_back = slCreateBox();
	slSetBoxDims(max_size_back,0.1,0.575,0.8,0.1,2);
	slRelBoxDims(max_size_back,settings_panel);

	max_size_mark = slCreateBox();
	slSetBoxDims(max_size_mark,0,0,0.1,1,1);
	slRelBoxDims(max_size_mark,max_size_back);
	max_size_mark->backcolor = {0,0,0,255};
	max_size_mark->bordercolor = {255,255,255,255};

	max_size_back->xy.y += max_size_back->wh.h * 0.5f;
	max_size_back->wh.h = 0;
	max_size_back->bordercolor = {255,255,255,255};
	max_size_back->backcolor = {255,255,255,255};

	max_size_slider = slCreateSlider(max_size_back,max_size_mark);
	max_size_slider->onchange = on_minmax_change;
	max_size_slider->minvalue = 6;
	max_size_slider->maxvalue = 40;



	min_size_slider->SetValue(min_grid_size);
	max_size_slider->SetValue(max_grid_size);
	on_minmax_change(min_size_slider);
	on_minmax_change(max_size_slider);



	settings_close = slCreateBox(slRenderText("Back"));
	slSetBoxDims(settings_close,0.4,0.8,0.2,0.1,2);
	settings_close->bordercolor = {191,191,191,255};
	settings_close->hoverbordercolor = {255,255,255,255};
	settings_close->drawmask = {191,191,191,255};
	settings_close->hoverable = true;
	settings_close->onclick = CloseSettings;
	slRelBoxDims(settings_close,settings_panel);



	settings_set_visi(false);
	opSetAppOptsCallback(settings_set_visi);
	opSetCustomEscapeCallback(op_try_escape);

	slBox* OpenMenuMain = slCreateBox();
	OpenMenuMain->hoverable = true;
	OpenMenuMain->backcolor = {0,0,0,63};
	OpenMenuMain->bordercolor = {191,191,191,255};
	OpenMenuMain->hoverbordercolor = {255,255,255,255};
	slSetBoxDims(OpenMenuMain,0.43,0.925,0.04,0.05,98);
	slBox* OpenMenuIcon = slCreateBox(slLoadTexture("resource/settings-icon.png"));
	slSetBoxDims(OpenMenuIcon,0,0,1,1,99);
	slRelBoxDims(OpenMenuIcon,OpenMenuMain);
	OpenMenuMain->onclick = opOpen;

	slBox* SkipButtonMain = slCreateBox();
	slSetBoxDims(SkipButtonMain,0.48,0.925,0.09,0.05,98);
	slBox* SkipButtonIcon = slCreateBox(slLoadTexture("resource/skip-icon.png"));
	slSetBoxDims(SkipButtonIcon,0,0,0.4,1,99);
	slRelBoxDims(SkipButtonIcon,SkipButtonMain);
	slBox* SkipButtonText = slCreateBox(slRenderText("Skip"));
	slSetBoxDims(SkipButtonText,0.4,0,0.6,1,99);
	slRelBoxDims(SkipButtonText,SkipButtonMain);
	SkipButtonMain->bordercolor = {191,191,191,255};
	SkipButtonMain->backcolor = {0,0,0,63};
	SkipButtonMain->onclick = OnSkipReq;
	SkipButtonMain->hoverable = true;
	SkipButtonMain->hoverbordercolor = {255,255,255,255};
	/*slBox* SkipButtonRed = slCreateBox();
	slSetBoxDims(SkipButtonRed,0,0,1,1,100);
	slRelBoxDims(SkipButtonRed,SkipButtonMain);
	SkipButtonRed->hoverbackcolor = {255,0,0,255};
	SkipButtonRed->hoverable = true;*/

	while (!slGetReqt())
	{
		timerbox = NULL;
		SkipReq = false;
		WinBox = NULL;
		GridW = min_grid_size + rand() % ((max_grid_size - min_grid_size) + 1);
		GridH = min_grid_size + rand() % ((max_grid_size - min_grid_size) + 1);
		HorizontalConns = malloc((GridW - 1) * GridH);
		VerticalConns = malloc(GridW * (GridH - 1));
		GenConns();
		tileids = malloc(GridW * GridH);
		rots = malloc(GridW * GridH);
		boxes = malloc(sizeof(slBox*) * GridW * GridH);
		clickboxes = malloc(sizeof(slBox*) * GridW * GridH);
		slScalar larger = GridW > GridH ? GridW : GridH;
		slScalar wh = 1 / larger;
		slScalar xplus = GridW > GridH ? 0 : wh * ((GridH - GridW) / 2.);
		slScalar yplus = GridW < GridH ? 0 : wh * ((GridW - GridH) / 2.);
		slBU cur = 0;
		slBox container;
		slSetBoxDims(&container,0.1,0.1,0.8,0.8,0);
		for (slBU y = 0; y < GridH; y++) for (slBU x = 0; x < GridW; x++, cur++)
		{
            char* tile_texpath = CalcTile(x,y,tileids + cur);
            slTexRef tile_tex = tile_texpath
                ? (slTexRef)slLoadTexture(tile_texpath)
                : (slTexRef)slNoTexture;
			slBox* box = slCreateBox(tile_tex);
			slSetBoxDims(box,(x / larger) + xplus,(y / larger) + yplus,wh,wh,170);
			slRelBoxDims(box,&container);
			box->maintainaspect = true;
			rots[cur] = rand() & 3;
			slSetBoxRots(box,true,rots[cur] * 90);
			boxes[cur] = box;
            box->userdata = NULL;
			slBox* clickbox = slCreateBox();
			clickbox->onclick = RotateBox_CW;
            clickbox->onrightclick = RotateBox_CCW;
			slSetBoxDims(clickbox,0,0,1,1,170);
			slRelBoxDims(clickbox,box);
			clickbox->maintainaspect = true;
			clickboxes[cur] = clickbox;
            clickbox->userdata = cur;
		};

		slBox* whitebox = slCreateBox();
		whitebox->backcolor = {255,255,255,255};
		slSetBoxDims(whitebox,0,0,1,1,64);
		slScalar t = 0;
		while (t < 1)
		{
			t += slGetDelta();
			whitebox->backcolor.a = slClamp255(1 - t);
			slCycle();
			if (slGetReqt()) break;
		};
		slDestroyBox(whitebox);
		WinConfirmed = false;
		while (!slGetReqt())
		{
			slCycle();
			if (timerbox)
			{
				Uint64 end = WinBox ? timerend : SDL_GetPerformanceCounter();
				slScalar elapsed = (end - timerstart) / (slScalar)SDL_GetPerformanceFrequency();
				char* timertext;
				asprintf(&timertext,"Elapsed Time: %.0f Seconds",(float)elapsed);
                timerbox->SetTexRef(slRenderText(timertext));
				free(timertext);
			};
			StepRotators();
			if (WinBox)
			{
				Rotator* last = NULL;
				for (slBU cur = 0; cur < Rotators.itemcount; cur++)
				{
					Rotator* rotator = Rotators.items[cur];
					if (last) if (rotator->age > last->age) continue;
					last = rotator;
				};
				WinBox->backcolor.a = last ? slClamp255(last->age) : 255;
			};
			if (WinConfirmed) break;
			if (SkipReq && !Rotators.itemcount) break;
		};
		free(HorizontalConns);
		free(VerticalConns);
		free(tileids);
		free(rots);
		if (!slGetReqt())
		{
			whitebox = slCreateBox();
			whitebox->backcolor = {255,255,255,0};
			slSetBoxDims(whitebox,0,0,1,1,64);
			t = 0;
			while (t < 1)
			{
				t += slGetDelta();
				whitebox->backcolor.a = slClamp255(t);
				slCycle();
				if (slGetReqt()) break;
			};
			slDestroyBox(whitebox);
		};
		if (WinBox)
		{
			slDestroyBox(WinBox);
			slDestroyBox(NextBox);
		};
		slBU total = GridW * GridH;
		for (cur = 0; cur < total; cur++)
		{
			slDestroyBox(boxes[cur]);
			slDestroyBox(clickboxes[cur]);
		};
		free(boxes);
		free(clickboxes);
		if (timerbox) slDestroyBox(timerbox);
        Rotators.Clear(free);
	};

	gridinfo = fopen("gridinfo","wb");
	if (gridinfo)
	{
		fwrite(&min_grid_size,1,1,gridinfo);
		fwrite(&max_grid_size,1,1,gridinfo);
		fclose(gridinfo);
	};

	fpsQuit();
	opQuit();
	slQuit();
};
