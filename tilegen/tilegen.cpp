#include "slice.h"
SDL_Color pixelat (slScalar x, slScalar y)
{
	x-=.5; y-=.5;
	slScalar dist = pow(x*x+y*y,.5);
	/*if (dist < 0.345 || dist > 0.455) return {0,0,0,0};
	else if (dist < 0.35 || dist > 0.45)
	{
		slScalar value = 255*(1-(fabs(fabs(dist-.4)-.05)/.005));
		return {0,0,0,value};
	}
	else*/ if (dist < 0.225 || dist > 0.275) return {0,0,0,0};
	else if (dist < 0.23 || dist > 0.27)
	{
		slScalar value = 255*(1-(fabs(fabs(dist-.25)-.02)/.005));
		return {255,255,255,value};
	}
	else return {255,255,255,255};
};
void render (Uint8* pixels, slBU w, slBU h)
{
	for (slBU y = 0; y < h; y++)
	{
		slScalar yvalue = y / (slScalar)(h - 1);
		for (slBU x = 0; x < w; x++)
		{
			SDL_Color color = pixelat(x / (slScalar)(w - 1),yvalue);
			*pixels = color.a;
			pixels++;
			*pixels = color.b;
			pixels++;
			*pixels = color.g;
			pixels++;
			*pixels = color.r;
			pixels++;
		};
	};
};
int main ()
{
	slInit();
	Uint8* pixels = malloc(256*256*4);
	render(pixels,256,256);
	slBox* box = slCreateBox(slCreateCustomTexture(256,256,pixels,true));
	free(pixels);
	slSetBoxDims(box,0,0,1,1,0);
	slBox* backbox = slCreateBox();
	backbox->backcolor = {255,0,0,255};
	slSetBoxDims(backbox,0,0,1,1,1);
	while (!slGetExitReq()) slCycle();
	SDL_Surface* surf = SDL_CreateRGBSurfaceFrom(pixels,256,256,32,1024,0xFF000000,0x00FF0000,0x0000FF00,0x000000FF);
	IMG_SavePNG(surf,"stub.png");
	slQuit();
};
